#include <string>
#include <vector>
using namespace std;
template<typename T>
class Stack
{
public:
	void push(T);
	T pop();
	void show();
private:
	vector<T> v;
};

template<class T>
void Stack<T> ::push(T elem)
{
	v.push_back(elem);
}

template<class T>
T Stack<T> ::pop()
{
	T elem = v.back();
	v.pop_back();
	return elem;
}

template<class T>
void Stack<T> ::show()
{
	cout << "Stack: ";
	for (auto e : v) cout << e << ' ';
	cout << endl;
}
