﻿#include <iostream>
#include "Chapter_18.h"
#include <string>
using namespace std;

int main()
{
	Stack<int> st;
	st.push(1); st.push(2); st.push(3);
	st.show();
	cout << "Popped: " << st.pop() << endl;
	st.show();
	Stack<string> str;
	str.push("Hello"); str.push("world");
	str.show();
	cout << "Popped:" << str.pop() << endl;
	str.show();
}

